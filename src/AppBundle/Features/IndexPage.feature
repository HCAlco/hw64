# language: ru

Функционал: Тестируем сайт цеха переводчиков

  Сценарий: Авторизоваться и добавить 10 популярных фраз
    Допустим я нахожусь на странице входа
    И я войду как "originalMan" с паролем "123321"
    Также я нахожусь на главной странице
    И добавлю популярную фразу "Чемпионат Мира 2018"
    И добавлю популярную фразу "Спиннер"
    И добавлю популярную фразу "Смотреть фильмы онлайн"
    И добавлю популярную фразу "Как начать майнить"
    И добавлю популярную фразу "Что такое хайп"
    И добавлю популярную фразу "Это фиаско, братан"
    И добавлю популярную фразу "Смерть Avicii"
    И добавлю популярную фразу "Оскар 2018"
    И добавлю популярную фразу "Скачать без регистрации"
    И добавлю популярную фразу "Новости"

  Сценарий: Добавить переводы для ранее добавленных фраз через одного пользователя
    Допустим я нахожусь на странице входа
    И я войду как "Vasya" с паролем "123321"
    Также я нахожусь на главной странице
    И добавлю перевод для "Чемпионат Мира 2018" англ: "World Championship 2018" порт: "Campeonato do Mundo 2018" фр:"Championnat du monde 2018" исп:"Campeonato Mundial 2018" нем:"Weltmeisterschaft 2018"
    И добавлю перевод для "Спиннер" англ: "Spinner" порт: "Girador" фр:"Fileur" исп:"Hilandero" нем:"Spinnere"
    И добавлю перевод для "Смотреть фильмы онлайн" англ: "Watch movies online" порт: "Assista Filmes Online" фр:"Regarder des films en ligne" исп:"Mira películas en línea" нем:"Sehen Sie sich Filme online an"
    И добавлю перевод для "Как начать майнить" англ: "How to Start mining" порт: "Como começar a mineração" фр:"Comment démarrer l'exploitation minière" исп:"Cómo comenzar a minar" нем:"So starten Sie das Mining"
    И добавлю перевод для "Что такое хайп" англ: "What is hype" порт: "o que é hype" фр:"quel est le battage" исп:"que es bombo" нем:"Was ist Hype?"

  Сценарий: Добавить переводы для ранее добавленных фраз через второго пользователя
    Допустим я нахожусь на странице входа
    И я войду как "Aibek" с паролем "123321"
    Также я нахожусь на главной странице
    И добавлю перевод для "Это фиаско, братан" англ: "It's a fiasco, bro" порт: "É um fiasco, mano" фр:"C'est un fiasco, mon frère" исп:"Es un fiasco, hermano" нем:"Es ist ein Fiasko, Bruder"
    И добавлю перевод для "Смерть Avicii" англ: "Death of Avicii" порт: "Morte de Avicii" фр:"Mort d'Avicii" исп:"Muerte de Avicii" нем:"Tod von Avicii"
    И добавлю перевод для "Оскар 2018" англ: "Oscar of 2018" порт: "Oscar de 2018" фр:"Oscar de 2018" исп:"Oscar de 2018" нем:"Oscar von 2018"
    И добавлю перевод для "Скачать без регистрации" англ: "Download without registration" порт: "Download sem registro" фр:"Télécharger sans inscription" исп:"Descargar sin registrarse" нем:"Download ohne Registrierung"
    И добавлю перевод для "Новости" англ: "News" порт: "Notícias" фр:"Actualités" исп:"Noticias" нем:"Nachrichten"
