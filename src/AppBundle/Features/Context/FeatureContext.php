<?php

namespace AppBundle\Features\Context;

use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements KernelAwareContext
{
    /** @var  KernelInterface */
    private $kernel;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     */
    public function яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('app_translation_index'));
    }

    /**
     * @When /^я нахожусь на странице входа$/
     */
    public function яНахожусьНаСтраницеВхода()
    {
        $this->visit($this->getContainer()->get('router')->generate('fos_user_security_login'));
    }

    /**
     * @When /^я войду как "([^"]*)" с паролем "([^"]*)"$/
     */
    public function яВойдуКак($arg1, $arg2)
    {
        $this->fillField('username', $arg1);
        $this->fillField('password', $arg2);
        $this->pressButton('_submit');
    }

    /**
     * @When /^добавлю популярную фразу "([^"]*)"$/
     */
    public function добавлюПопулярнуюФразу($arg1)
    {
        $this->fillField('form_phrase', $arg1);
        $this->pressButton('form_save');
    }

    /**
     * @When /^добавлю перевод для "([^"]*)" англ: "([^"]*)" порт: "([^"]*)" фр:"([^"]*)" исп:"([^"]*)" нем:"([^"]*)"$/
     */
    public function добавлюПереводДляСлова($phrase, $en, $pt, $fr, $es, $de)
    {
        $this->clickLink($phrase);
        $this->fillField('en', $en);
        $this->fillField('pt', $pt);
        $this->fillField('fr', $fr);
        $this->fillField('es', $es);
        $this->fillField('de', $de);
        $this->pressButton('buton');
        $this->visit($this->getContainer()->get('router')->generate('app_translation_index'));
    }

    /**
     * Sets Kernel instance.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }
}
