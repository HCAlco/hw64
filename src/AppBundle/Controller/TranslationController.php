<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Phrase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class TranslationController extends Controller
{
    /**
     * @Route("/")
     * @Method({"GET","HEAD", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {

        dump($request->getLocale());
        $phrases = $this->getDoctrine()
            ->getRepository('AppBundle:Phrase')
            ->findAll();
        $save = 'Отправить';
        if ($request->getLocale() === 'en') {
            $save = 'Submit';
        }
        $form = $this->createFormBuilder()
            ->add('phrase', TextAreaType::class, ['label' => 'phrase'])
            ->add('save', SubmitType::class, ['label' => $save, 'attr' => array('class' => 'btn btn-primary')])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $phrase = new Phrase();
            $phrase->translate('ru')->setPhrase($data['phrase']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($phrase);

            $phrase->mergeNewTranslations();
            $em->flush();
            return $this->redirectToRoute('app_translation_index');
        }

        return $this->render('@App/Translation/index.html.twig', array(
            'form' => $form->createView(),
            'phrases' => $phrases,
        ));
    }

    /**
     * @Route("/phrase//save/{id}")
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function saveAction(Request $request, int $id)
    {
        $codes = ['en', 'pt', 'fr', 'es', 'de'];
        $phrase = $this->getDoctrine()->getRepository('AppBundle:Phrase')->find($id);

        $em = $this->getDoctrine()->getManager();
        for ($i = 0; $i < count($codes); $i++) {
            if (!empty($request->get($codes[$i]))) {
                $phrase->translate($codes[$i], false)->setPhrase($request->get($codes[$i]));
            };
        }
        $em->persist($phrase);
        $phrase->mergeNewTranslations();
        $em->flush();

        return $this->redirectToRoute('app_translation_view', array(
            'id' => $id
        ));
    }

    /**
     * @Route("/phrase/{id}")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(int $id)
    {
        $rep = $this->getDoctrine()
            ->getRepository('AppBundle:Phrase');
        $phrase = $rep->find($id);
        return $this->render('@App/Translation/view.html.twig', array(
            'phrase' => $phrase,
        ));
    }

    /**
     * @Route("/{_locale}/", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function languageAction(Request $request)
    {
        return $this->redirect($request->headers->get('referer'));
    }

}
