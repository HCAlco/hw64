<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUsersData implements FixtureInterface, ORMFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $password = password_hash('123321', PASSWORD_DEFAULT);
        $user
            ->setUsername('originalMan')
            ->setEmail('original@some.com')
            ->setPassword($password)
            ->setEnabled(1);
        $manager->persist($user);

        $user1 = new User();
        $password = password_hash('123321', PASSWORD_DEFAULT);
        $user1
            ->setUsername('Vasya')
            ->setEmail('Vasya@Pupkin.com')
            ->setPassword($password)
            ->setEnabled(1);
        $manager->persist($user1);

        $user2 = new User();
        $password = password_hash('123321', PASSWORD_DEFAULT);
        $user2
            ->setUsername('Aibek')
            ->setEmail('koshoev.aibek@mail.com')
            ->setPassword($password)
            ->setEnabled(1);
        $manager->persist($user2);

        $manager->flush();
    }

}
